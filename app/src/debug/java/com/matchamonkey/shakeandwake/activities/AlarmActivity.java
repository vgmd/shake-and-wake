package com.matchamonkey.shakeandwake.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.matchamonkey.shakeandwake.AlarmController;
import com.matchamonkey.shakeandwake.AlarmReceiver;
import com.matchamonkey.shakeandwake.BuildConfig;
import com.matchamonkey.shakeandwake.Helper;
import com.matchamonkey.shakeandwake.R;
import com.matchamonkey.shakeandwake.WakeLockManager;

import java.util.Calendar;

public class AlarmActivity extends Activity implements SensorEventListener {

    private static final int EVENT_THRESHOLD = 100; // time in ms to check for shake events

    private long mLastUpdate;
    private float mLastX = 0.0f;
    private float mLastY = 0.0f;
    private float mLastZ = 0.0f;
    private long mTimeShaken = 0;
    private float mVolume;

    private Button mBtnTurnOff;
    private MediaPlayer mMediaPlayer;
    private RelativeLayout mLayout;
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private TextView mTextView;
    private Uri mAlarmURI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);

        // Turn screen on and keep phone awake.
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_FULLSCREEN |
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

        mLayout = (RelativeLayout) findViewById(R.id.layout_alarm);
        mTextView = (TextView) findViewById(R.id.text_alarm);

        mBtnTurnOff = (Button) findViewById(R.id.btn_turn_off);
        mBtnTurnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (getActionBar() != null)
            getActionBar().hide();

        // Get the saved ringtone URI (or default one)
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            mAlarmURI = Uri.parse(sp.getString(SettingsActivity.KEY_PREF_RINGTONE, null));
        } catch (NullPointerException e) {
            mAlarmURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        }

        // Get the user's volume settings from shared preferences
        mVolume = ((float) sp.getInt(SettingsActivity.KEY_PREF_VOLUME, 50)) / 100.0f;
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_GAME);

        mLastUpdate = System.currentTimeMillis();

        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
        try {
            mMediaPlayer.setDataSource(this, mAlarmURI);
            mMediaPlayer.prepare();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        mMediaPlayer.setVolume(mVolume, mVolume);
        mMediaPlayer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();

        mSensorManager.unregisterListener(this);

        int alarmID = getIntent().getIntExtra(AlarmController.EXTRA_ALARM_ID, -1);
        if (alarmID >= 0) {
            AlarmController alarmController = AlarmController.get(this, Calendar.getInstance());
            alarmController.alarmFinished(alarmID, Calendar.getInstance());
        }

        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }

        WakeLockManager.release();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (mSensor == event.sensor) {
            final int BG_COLOR = getResources().getColor(R.color.list_background);

            // calculate the shake threshold based on user's settings
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
            final float SHAKE_THRESHOLD = 2000.0f * ((float) sp.getInt(SettingsActivity.KEY_PREF_INTENSITY, 50)) / 100.0f + 3000.0f; // Shake threshold can be in the range of 3000.0f - 5000.0f

            long currTime = System.currentTimeMillis();
            if ((currTime - mLastUpdate) > EVENT_THRESHOLD) {
                final long TIME_TO_SHAKE = (long) ((4.0f * ((float) sp.getInt(SettingsActivity.KEY_PREF_DURATION, 50)) / 100.0f + 1.0f) * 1000f); // Shake duration in the range of 1.0s - 5.0s
                long diffTime = currTime - mLastUpdate;

                mLastUpdate = currTime;

                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];

                float speed = Math.abs(x + y + z - mLastX - mLastY - mLastZ) / diffTime * 10000;

                if (speed >= SHAKE_THRESHOLD) {
                    mTimeShaken += diffTime;
                    if (mTimeShaken >= TIME_TO_SHAKE) {
                        finish();
                    }
                } else {
                    mTimeShaken -= diffTime / 2; // Not shaking the phone makes progress go down by half the amount of time
                    if (mTimeShaken < 0)
                        mTimeShaken = 0;
                    mLayout.setBackgroundColor(Helper.getValueAdjustedColor(BG_COLOR, (float) mTimeShaken / TIME_TO_SHAKE));
                }

                float ratioComplete = (float) mTimeShaken / TIME_TO_SHAKE;
                mTextView.setText(getString(R.string.title_activity_alarm));
                mLayout.setBackgroundColor(Helper.getValueAdjustedColor(BG_COLOR, ratioComplete));
                if (mMediaPlayer != null)
                    mMediaPlayer.setVolume(mVolume * (1 - ratioComplete), mVolume * (1 - ratioComplete));

                mLastX = x;
                mLastY = y;
                mLastZ = z;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onBackPressed() {

    }
}
