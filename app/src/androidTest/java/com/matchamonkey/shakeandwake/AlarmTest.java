package com.matchamonkey.shakeandwake;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Calendar;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class AlarmTest {

    private Calendar cal;

    @Before
    public void setUp() {
        cal = Calendar.getInstance();
    }

    @Test
    public void Alarm_MillisToNextAlarm() {
        // Set the calendar for Friday, January 15, 2016, 10:30 AM
        cal.set(2016, Calendar.JANUARY, 15, 10, 30, 0);
        cal.set(Calendar.MILLISECOND, 0);
        System.out.println("Month: " + cal.get(Calendar.MONTH));

        // Alarm set for 10:30
        Alarm alarm = new Alarm(10, 30, 0, 0);
        assertEquals(cal.getTimeInMillis(), alarm.millisToNextAlarm(cal, false));

        // Alarm set for 11:30
        alarm = new Alarm(11, 30, 0, 0);
        assertEquals(cal.getTimeInMillis() + 3600000L, alarm.millisToNextAlarm(cal, false));

        // Alarm set for 9:30
        alarm = new Alarm(9, 30, 0, 0);
        assertEquals(cal.getTimeInMillis() + 82800000L, alarm.millisToNextAlarm(cal, false));

        // Alarm set for 10:35
        alarm = new Alarm(10, 35, 0, 0);
        assertEquals(cal.getTimeInMillis() + 300000L, alarm.millisToNextAlarm(cal, false));

        // Alarm set for 10:25
        alarm = new Alarm(10, 25, 0, 0);
        assertEquals(cal.getTimeInMillis() + 86100000L, alarm.millisToNextAlarm(cal, false));

        // Alarm set for 00:25
        alarm = new Alarm(0, 25, 0, 0);
        assertEquals(cal.getTimeInMillis() + 50100000L, alarm.millisToNextAlarm(cal, false));

        // Alarm set for Saturday and Sunday, 11:30 a.m.
        alarm = new Alarm(11, 30, 0, Alarm.SATURDAY | Alarm.SUNDAY);
        assertEquals(cal.getTimeInMillis() + 90000000L, alarm.millisToNextAlarm(cal, false));

        // Alarm set for Monday through Friday, 11:00 a.m.
        alarm = new Alarm(11, 0, 0, Alarm.MONDAY | Alarm.TUESDAY | Alarm.WEDNESDAY | Alarm.THURSDAY | Alarm.FRIDAY);
        assertEquals(cal.getTimeInMillis() + 1800000L, alarm.millisToNextAlarm(cal, false));

        // Alarm set for Friday and Sunday, 10:27 a.m.
        alarm = new Alarm(10, 27, 0, Alarm.SUNDAY | Alarm.FRIDAY);
        assertEquals(cal.getTimeInMillis() + 172620000L, alarm.millisToNextAlarm(cal, false));

        // Alarm set for 10:30 p.m.
        alarm = new Alarm(22, 30, 0, 0);
        assertEquals(cal.getTimeInMillis() + 43200000L, alarm.millisToNextAlarm(cal, false));
    }
}
