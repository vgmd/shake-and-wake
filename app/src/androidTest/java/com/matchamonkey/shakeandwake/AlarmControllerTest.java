package com.matchamonkey.shakeandwake;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Calendar;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class AlarmControllerTest {

    @Test
    public void alarmController_createAlarm() {
        AlarmController alarmController = AlarmController.get(InstrumentationRegistry.getTargetContext(), Calendar.getInstance());
        int id = alarmController.createAlarm(0, 0, 0, Calendar.getInstance());
        Alarm a = alarmController.getAlarm(id);
        assertNotNull(a);
        alarmController.deleteAlarm(id, Calendar.getInstance());
    }

    @Test
    public void alarmController_getEarliestAlarm() {
        AlarmController alarmController = AlarmController.get(InstrumentationRegistry.getTargetContext(), Calendar.getInstance());
        Calendar cal = Calendar.getInstance();
        cal.set(2016, Calendar.SEPTEMBER, 8, 22, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Alarm alarm1 = alarmController.getAlarm(alarmController.createAlarm(9, 0, Alarm.MONDAY | Alarm.TUESDAY | Alarm.WEDNESDAY | Alarm.THURSDAY | Alarm.FRIDAY, cal));
        Alarm alarm2 = alarmController.getAlarm(alarmController.createAlarm(10, 0, Alarm.SUNDAY | Alarm.SATURDAY, cal));
        cal.set(2016, Calendar.SEPTEMBER, 9, 1, 0);
        Alarm alarm3 = alarmController.getAlarm(alarmController.createAlarm(8, 15, 0, cal));

        assertSame(alarm3, alarmController.getEarliestAlarm(cal));

        // first alarm (alarm3) goes off
        cal.set(2016, Calendar.SEPTEMBER, 9, 8, 15);
        assertEquals(cal.getTimeInMillis(), alarm3.millisToNextAlarm(cal, false));
        alarmController.alarmFinished(alarm3.getID(), cal);

        cal.set(2016, Calendar.SEPTEMBER, 9, 8, 16);
        assertSame(alarm1, alarmController.getEarliestAlarm(cal));
    }
}
