package com.matchamonkey.shakeandwake;

import android.content.Context;
import android.os.PowerManager;

public class WakeLockManager {

    private static PowerManager.WakeLock sWakeLock;

    public static final String WAKE_LOCK_TAG = "ShakeAndWakeWakeLock";

    public static void acquire(Context context) {
        if (sWakeLock == null) {
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            sWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, WAKE_LOCK_TAG);
        }
        sWakeLock.acquire();
    }

    public static void release() {
        if (sWakeLock != null && sWakeLock.isHeld())
            sWakeLock.release();
    }

}
