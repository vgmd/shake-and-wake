package com.matchamonkey.shakeandwake.fragments;

import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.View;

import com.matchamonkey.shakeandwake.R;
import com.matchamonkey.shakeandwake.activities.SettingsActivity;

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    public SettingsFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onResume() {
        super.onResume();

        PreferenceManager.getDefaultSharedPreferences(getActivity())
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();

        PreferenceManager.getDefaultSharedPreferences(getActivity())
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Preference pref = findPreference(SettingsActivity.KEY_PREF_RINGTONE);
        if (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(SettingsActivity.KEY_PREF_RINGTONE, null) != null) {
            Uri ringtoneURI = Uri.parse(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(SettingsActivity.KEY_PREF_RINGTONE, null));
            String ringtoneTitle = RingtoneManager.getRingtone(getActivity(), ringtoneURI).getTitle(getActivity());
            pref.setSummary(ringtoneTitle);
        } else {
            pref.setSummary(R.string.default_ringtone_not_found);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch(key) {
            case SettingsActivity.KEY_PREF_RINGTONE:
                Preference pref = findPreference(SettingsActivity.KEY_PREF_RINGTONE);
                Uri ringtoneURI = Uri.parse(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(SettingsActivity.KEY_PREF_RINGTONE, null));
                String ringtoneTitle = RingtoneManager.getRingtone(getActivity(), ringtoneURI).getTitle(getActivity());
                pref.setSummary(ringtoneTitle);
                break;
        }
    }
}
