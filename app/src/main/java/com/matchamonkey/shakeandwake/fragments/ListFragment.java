package com.matchamonkey.shakeandwake.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.matchamonkey.shakeandwake.Alarm;
import com.matchamonkey.shakeandwake.AlarmController;
import com.matchamonkey.shakeandwake.AlarmReceiver;
import com.matchamonkey.shakeandwake.R;
import com.matchamonkey.shakeandwake.activities.AlarmSetupActivity;

import java.util.ArrayList;
import java.util.Calendar;

public class ListFragment extends android.app.ListFragment {

    public int REQ_NEW_ALARM = 0;
    public int REQ_EDIT_ALARM = 1;

    private ImageButton mBtnAdd;
    private AlarmAdapter mAlarmAdapter;

    private class AlarmAdapter extends ArrayAdapter<Alarm> {

        public AlarmAdapter(ArrayList<Alarm> alarms) {
            super(getActivity(), 0, alarms);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null)
                convertView = getActivity().getLayoutInflater().inflate(R.layout.list_item_alarm, parent, false);

            final Alarm a = getItem(position);

            // Configure Time TextView
            boolean isAM;
            if (a.getHour() >= 12)
                isAM = false;
            else
                isAM = true;

            StringBuilder sb = new StringBuilder();
            if (a.getHour() > 12)
                sb.append(a.getHour() - 12);
            else if (a.getHour() == 0)
                sb.append(12);
            else
                sb.append(a.getHour());
            sb.append(":");
            if (a.getMin() < 10)
                sb.append("0");
            sb.append(a.getMin());

            TextView textTime = (TextView) convertView.findViewById(R.id.text_time);
            textTime.setText(sb.toString());

            // Configure AM/PM TextView
            TextView textAMPM = (TextView) convertView.findViewById(R.id.text_ampm);
            if (isAM)
                textAMPM.setText("AM");
            else
                textAMPM.setText("PM");

            // Configure ToggleButton
            ToggleButton btn_set = (ToggleButton) convertView.findViewById(R.id.btn_toggle);
            btn_set.setChecked(a.isSet());
            btn_set.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlarmController.get(getActivity(), Calendar.getInstance()).toggleAlarm(a.getID(), Calendar.getInstance());
                }
            });

            // configure Delete Button
            ImageButton btn_delete = (ImageButton) convertView.findViewById(R.id.btn_delete);
            btn_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlarmController.get(getActivity(), Calendar.getInstance()).deleteAlarm(a.getID(), Calendar.getInstance());
                    mAlarmAdapter.notifyDataSetChanged();
                }
            });

            // Configure Days of the Week
            int days = a.getDays();
            TextView textSu = (TextView) convertView.findViewById(R.id.text_su);
            TextView textM = (TextView) convertView.findViewById(R.id.text_m);
            TextView textTu = (TextView) convertView.findViewById(R.id.text_tu);
            TextView textW = (TextView) convertView.findViewById(R.id.text_w);
            TextView textTh = (TextView) convertView.findViewById(R.id.text_th);
            TextView textF = (TextView) convertView.findViewById(R.id.text_f);
            TextView textSa = (TextView) convertView.findViewById(R.id.text_sa);

            if ((days & Alarm.SUNDAY) > 0) {
                textSu.setTextColor(getResources().getColor(R.color.list_background));
            } else
                textSu.setTextColor(getResources().getColor(R.color.light_gray));
            if ((days & Alarm.MONDAY) > 0) {
                textM.setTextColor(getResources().getColor(R.color.list_background));
            } else
                textM.setTextColor(getResources().getColor(R.color.light_gray));
            if ((days & Alarm.TUESDAY) > 0) {
                textTu.setTextColor(getResources().getColor(R.color.list_background));
            } else
                textTu.setTextColor(getResources().getColor(R.color.light_gray));
            if ((days & Alarm.WEDNESDAY) > 0) {
                textW.setTextColor(getResources().getColor(R.color.list_background));
            } else
                textW.setTextColor(getResources().getColor(R.color.light_gray));
            if ((days & Alarm.THURSDAY) > 0) {
                textTh.setTextColor(getResources().getColor(R.color.list_background));
            } else
                textTh.setTextColor(getResources().getColor(R.color.light_gray));
            if ((days & Alarm.FRIDAY) > 0) {
                textF.setTextColor(getResources().getColor(R.color.list_background));
            } else
                textF.setTextColor(getResources().getColor(R.color.light_gray));
            if ((days & Alarm.SATURDAY) > 0) {
                textSa.setTextColor(getResources().getColor(R.color.list_background));
            } else
                textSa.setTextColor(getResources().getColor(R.color.light_gray));

            return convertView;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        AlarmController.get(getActivity(), Calendar.getInstance()).saveAlarms();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_list, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent i = new Intent(getActivity(), com.matchamonkey.shakeandwake.activities.SettingsActivity.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View root = getView();
        mBtnAdd = (ImageButton) root.findViewById(R.id.btn_add);
        mBtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AlarmSetupActivity.class);
                startActivityForResult(i, REQ_NEW_ALARM);
            }
        });

        mAlarmAdapter = new AlarmAdapter(AlarmController.get(getActivity(), Calendar.getInstance()).getAlarms());
        setListAdapter(mAlarmAdapter);

        if (AlarmController.get(getActivity(), Calendar.getInstance()).alarmIsSet())
            AlarmController.get(getActivity(), Calendar.getInstance()).showNotification(Calendar.getInstance());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mAlarmAdapter.notifyDataSetChanged();

        if (data == null)
            return;

        Log.d("ListActivity", "Alarm id is: " + data.getIntExtra(AlarmSetupActivity.EXTRA_ALARM_ID, -1));
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Alarm a = (Alarm) getListAdapter().getItem(position);

        Intent i = new Intent(getActivity(), AlarmSetupActivity.class);
        i.putExtra(AlarmSetupActivity.EXTRA_ALARM_ID, a.getID());
        startActivityForResult(i, REQ_EDIT_ALARM);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mBtnAdd = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        mAlarmAdapter.notifyDataSetChanged();
    }
}
