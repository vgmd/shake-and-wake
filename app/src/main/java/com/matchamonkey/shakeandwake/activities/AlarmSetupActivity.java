package com.matchamonkey.shakeandwake.activities;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.matchamonkey.shakeandwake.Alarm;
import com.matchamonkey.shakeandwake.AlarmController;
import com.matchamonkey.shakeandwake.R;

import java.util.Calendar;

public class AlarmSetupActivity extends Activity {

    public static final String EXTRA_ALARM_ID = "alarm_id";

    private TimePicker mTimePicker;
    private Button mBtnConfirm;
    private ToggleButton mBtnSu;
    private ToggleButton mBtnM;
    private ToggleButton mBtnTu;
    private ToggleButton mBtnW;
    private ToggleButton mBtnTh;
    private ToggleButton mBtnF;
    private ToggleButton mBtnSa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        final int alarmID = intent.getIntExtra(EXTRA_ALARM_ID, -1);

        mTimePicker = (TimePicker) findViewById(R.id.timePicker);
        mBtnSu = (ToggleButton) findViewById(R.id.btn_su);
        mBtnM = (ToggleButton) findViewById(R.id.btn_m);
        mBtnTu = (ToggleButton) findViewById(R.id.btn_tu);
        mBtnW = (ToggleButton) findViewById(R.id.btn_w);
        mBtnTh = (ToggleButton) findViewById(R.id.btn_th);
        mBtnF = (ToggleButton) findViewById(R.id.btn_f);
        mBtnSa = (ToggleButton) findViewById(R.id.btn_sa);

        mBtnConfirm = (Button) findViewById(R.id.btn_confirm);
        mBtnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int hour = mTimePicker.getCurrentHour();
                int min = mTimePicker.getCurrentMinute();

                int days = (mBtnSu.isChecked() ? Alarm.SUNDAY : 0)
                        | (mBtnM.isChecked() ? Alarm.MONDAY : 0)
                        | (mBtnTu.isChecked() ? Alarm.TUESDAY : 0)
                        | (mBtnW.isChecked() ? Alarm.WEDNESDAY : 0)
                        | (mBtnTh.isChecked() ? Alarm.THURSDAY : 0)
                        | (mBtnF.isChecked() ? Alarm.FRIDAY : 0)
                        | (mBtnSa.isChecked() ? Alarm.SATURDAY : 0);

                if (alarmID < 0) {
                    int newID = AlarmController.get(AlarmSetupActivity.this, Calendar.getInstance()).createAlarm(hour, min, days, Calendar.getInstance());
                    Intent i = new Intent();
                    i.putExtra(EXTRA_ALARM_ID, newID);
                    setResult(RESULT_OK, i);
                    finish();
                } else {
                    AlarmController.get(AlarmSetupActivity.this, Calendar.getInstance()).editAlarm(alarmID, hour, min, days);
                    finish();
                }
            }
        });

        setTitle(R.string.title_activity_alarm_setup);

        // Check if alarm data already exists and, if so, load that data into the view
        if (alarmID < 0)
            return;

        Alarm alarm = AlarmController.get(this, Calendar.getInstance()).getAlarm(alarmID);
        mTimePicker.setCurrentHour(alarm.getHour());
        mTimePicker.setCurrentMinute(alarm.getMin());
        int days = alarm.getDays();
        mBtnSu.setChecked((days & Alarm.SUNDAY) > 0);
        mBtnM.setChecked((days & Alarm.MONDAY) > 0);
        mBtnTu.setChecked((days & Alarm.TUESDAY) > 0);
        mBtnW.setChecked((days & Alarm.WEDNESDAY) > 0);
        mBtnTh.setChecked((days & Alarm.THURSDAY) > 0);
        mBtnF.setChecked((days & Alarm.FRIDAY) > 0);
        mBtnSa.setChecked((days & Alarm.SATURDAY) > 0);

        // Enable the Up button
        ActionBar ab = getActionBar();
        if (ab != null)
            ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        AlarmController.get(this, Calendar.getInstance()).saveAlarms();
    }
}
