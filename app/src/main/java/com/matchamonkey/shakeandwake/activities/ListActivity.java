package com.matchamonkey.shakeandwake.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.matchamonkey.shakeandwake.R;
import com.matchamonkey.shakeandwake.fragments.ListFragment;

public class ListActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        FragmentManager fm = getFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            fragment = new ListFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }

        // Set up default preferences that can't be set in xml file, like ringtone
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        if (sp.getString(SettingsActivity.KEY_PREF_RINGTONE, null) == null) {
            if (RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_ALARM) == null) {
                Log.e("ListActivity", "System does not have a default ringtone!");
            } else {
                sp.edit()
                        .putString(SettingsActivity.KEY_PREF_RINGTONE, RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_ALARM).toString())
                        .apply();
            }
        }
        PreferenceManager.setDefaultValues(this, R.xml.preferences, true);
    }
}
