package com.matchamonkey.shakeandwake.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;

import com.matchamonkey.shakeandwake.R;
import com.matchamonkey.shakeandwake.fragments.SettingsFragment;

public class SettingsActivity extends Activity {

    public static final String KEY_PREF_RINGTONE = "pref_ringtone";
    public static final String KEY_PREF_VOLUME = "pref_volume";
    public static final String KEY_PREF_INTENSITY = "pref_shake_intensity";
    public static final String KEY_PREF_DURATION = "pref_shake_duration";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        FragmentManager fm = getFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            fragment = new SettingsFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }
    }
}
