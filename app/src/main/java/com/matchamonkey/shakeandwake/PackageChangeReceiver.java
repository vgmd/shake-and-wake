package com.matchamonkey.shakeandwake;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

public class PackageChangeReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, PackageChangeService.class);
        i.setAction(intent.getAction());

        startWakefulService(context, i);
    }
}