package com.matchamonkey.shakeandwake;

import android.graphics.Color;

public class Helper {

    /**
     * Returns a color whose HSV "Value" has been multiplied by ratio.
     * @param color int representing an argb value
     * @param ratio 0.0f to 1.0f
     * @return int representing an argb value
     */
    public static int getValueAdjustedColor(int color, float ratio) {

        float color_hsv[] = new float[3];
        float result_hsv[] = new float[3];

        Color.colorToHSV(color, color_hsv);

        result_hsv[0] = color_hsv[0];
        result_hsv[1] = color_hsv[1];
        result_hsv[2] = color_hsv[2] * ratio;

        return Color.HSVToColor(result_hsv);
    }
}
