package com.matchamonkey.shakeandwake;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.matchamonkey.shakeandwake.activities.AlarmActivity;

public class AlarmReceiver extends BroadcastReceiver {

    public void onReceive(Context c, Intent i) {
        WakeLockManager.acquire(c);
        Intent intent = new Intent(c, AlarmActivity.class);
        intent.putExtra(AlarmController.EXTRA_ALARM_ID, i.getIntExtra(AlarmController.EXTRA_ALARM_ID, -1));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        c.startActivity(intent);
    }
}
