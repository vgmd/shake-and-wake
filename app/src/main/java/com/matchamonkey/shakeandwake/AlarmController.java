package com.matchamonkey.shakeandwake;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.matchamonkey.shakeandwake.activities.ListActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class AlarmController {

    private static final String TAG = "AlarmController";
    private static final String FILENAME = "alarms.json";
    public static final int MAX_ALARMS = 1000;
    public static final String EXTRA_ALARM_ID = "ALARM_ID";
    public static final String INTENT_ACTION = "com.matchamonkey.shakeandwake.ALARM";

    private static final int NOTIFICATION_ID = 1;

    private static AlarmController sAlarmController;
    private Context mAppContext;
    private ArrayList<Alarm> mAlarms;
    private AlarmManager mAlarmManager;
    private AlarmJSONSerializer mSerializer;

    /**
     * Gets a static instance of AlarmController.
     * @param c The application's context
     * @return an instance of AlarmController
     */
    public static AlarmController get(Context c, Calendar cal) {
        if (sAlarmController == null) {
            sAlarmController = new AlarmController(c.getApplicationContext(), cal);
        }
        return sAlarmController;
    }

    /**
     * Gets the alarm with the corresponding ID.
     * @param id The ID of the alarm.
     * @return The alarm with the corresponding ID, or null if none exists.
     */
    public Alarm getAlarm(int id) {
        for (int i = 0; i < mAlarms.size(); i++) {
            if (mAlarms.get(i).getID() == id) {
                return mAlarms.get(i);
            }
        }
        return null;
    }

    public ArrayList<Alarm> getAlarms() {
        return mAlarms;
    }

    /**
     * Create a new alarm.
     * @param hour The hour of the alarm.
     * @param min The minute of the alarm.
     * @param days Flags indicating which days of the week the alarm should be set.
     * @param calendar An instance of java.util.Calendar that represents the current time.
     * @return ID of newly created alarm.
     */
    public int createAlarm(int hour, int min, int days, Calendar calendar) {

        if (mAlarms.size() >= MAX_ALARMS) {
            Toast.makeText(mAppContext, "Cannot set more than " + MAX_ALARMS + " alarms!", Toast.LENGTH_LONG).show();
            return -1;
        }

        int id = generateAlarmID();
        if (id < 0) {
            // This should never happen because we check if there's enough space at the start of the method.
            Log.e("AlarmController", "Could not generate a new alarm id");
            System.exit(1);
        }

        // Add the new alarm and sort
        Alarm a = new Alarm(hour, min, id, days);

        mAlarms.add(a);
        Collections.sort(mAlarms);

        enableAlarm(a, false, calendar);

        return id;
    }

    /**
     * Edit an already existing alarm. Does nothing if alarm id doesn't exist.
     * @param id The ID of the alarm to edit.
     * @param hour The new hour of the alarm.
     * @param min The new minute of the alarm.
     * @param days New flags indicating the days which the alarm should be set.
     */
    public void editAlarm(int id, int hour, int min, int days) {
        Alarm alarm = getAlarm(id);
        if (alarm == null)
            return;

        alarm.setHour(hour);
        alarm.setMin(min);
        alarm.setDays(days);

        Collections.sort(mAlarms);

        enableAlarm(alarm, false, Calendar.getInstance());
    }

    public void toggleAlarm(int id, Calendar calendar) {
        Alarm a = getAlarm(id);
        if (a == null)
            return;

        if (a.isSet())
            disableAlarm(a, calendar);
        else
            enableAlarm(a, false, Calendar.getInstance());
    }

    public void deleteAlarm(int id, Calendar calendar) {
        for (int j = 0; j < mAlarms.size(); j++) {
            if (mAlarms.get(j).getID() == id) {
                disableAlarm(mAlarms.get(j), calendar);
                mAlarms.remove(j);
                break;
            }
        }
    }

    /**
     * Disables an alarm if non-repeating, else enable it again for the next day it's enabled.
     * @param id
     */
    public void alarmFinished(int id, Calendar calendar) {
        Alarm a = getAlarm(id);
        if (a == null)
            return;

        if (a.isSet())
            if (a.getDays() == 0)
                disableAlarm(a, calendar);
            else {
                // This is a repeating alarm, so set it for the next day it's enabled.
                enableAlarm(a, true, calendar);
            }
    }

    public boolean saveAlarms() {
        try {
            mSerializer.saveAlarms(mAlarms);
            Log.d(TAG, "alarms saved to file");
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Error saving alarms: ", e);
            return false;
        }
    }

    public void loadAlarms(Calendar cal) {
        mSerializer = new AlarmJSONSerializer(mAppContext, FILENAME);
        try {
            mAlarms = mSerializer.loadAlarms();
        } catch (Exception e) {
            mAlarms = new ArrayList<Alarm>();
            Log.e(TAG, "Error loading alarms: ", e);
        }
        for (Alarm alarm : mAlarms) {
            if (alarm.isSet()) {
                enableAlarm(alarm, false, cal);
            }
        }
    }

    public void showNotification(Calendar calendar) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mAppContext)
                .setSmallIcon(android.R.drawable.ic_lock_idle_alarm)
                .setContentTitle(mAppContext.getString(R.string.app_name))
                .setContentText(mAppContext.getString(R.string.alarm_set_for) + " " + getEarliestAlarm(calendar).toString(calendar))
                .setOngoing(true);

        Intent intent = new Intent(mAppContext, ListActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(mAppContext, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) mAppContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    public void cancelNotification() {
        NotificationManager notificationManager = (NotificationManager) mAppContext.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    /**
     * @return true or false depending on if there is at least one active alarm set or not
     */
    public boolean alarmIsSet() {
        for (int i = 0; i < mAlarms.size(); i++) {
            if (mAlarms.get(i).isSet())
                return true;
        }

        return false;
    }

    /**
     * @return The earliest alarm that is currently set, or null if no alarm is set
     * @param calendar An instance of java.util.Calendar that represents the current time.
     */
    public Alarm getEarliestAlarm(Calendar calendar) {
        // no alarms
        if (mAlarms.size() == 0)
            return null;

        // one alarm
        if (mAlarms.size() == 1)
            if (mAlarms.get(0).isSet())
                return mAlarms.get(0);
            else
                return null;

        // two or more alarms
        // Find the first set alarm and set it to earliest
        Alarm earliest = null;
        int i;
        for (i = 0; i < mAlarms.size(); i++) {
            if (mAlarms.get(i).isSet()) {
                earliest = mAlarms.get(i);
                break;
            }
        }
        if (earliest == null)
            return null; // None of the alarms are set

        for (int j = i + 1; j < mAlarms.size(); j++) {
            Alarm comparator = mAlarms.get(j);
            if (comparator.isSet() && comparator.millisToNextAlarm(calendar, false)
                    < earliest.millisToNextAlarm(calendar, false))
                earliest = comparator;
        }

        return earliest;
    }

    private AlarmController(Context appContext, Calendar cal) {
        mAppContext = appContext;
        mAlarmManager = (AlarmManager) mAppContext.getSystemService(Context.ALARM_SERVICE);
        loadAlarms(cal);
    }

    private void enableAlarm(Alarm a, boolean justFinished, Calendar calendar) {
        Intent i = new Intent(INTENT_ACTION);
        i.putExtra(EXTRA_ALARM_ID, a.getID());

        if (Build.VERSION.SDK_INT >= 23) {
            mAlarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, a.millisToNextAlarm(calendar, justFinished),
                    PendingIntent.getBroadcast(mAppContext, a.getID(), i, PendingIntent.FLAG_UPDATE_CURRENT));
        } else if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 23) {
            mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, a.millisToNextAlarm(calendar, justFinished),
                    PendingIntent.getBroadcast(mAppContext, a.getID(), i, PendingIntent.FLAG_UPDATE_CURRENT));
        } else {
            mAlarmManager.set(AlarmManager.RTC_WAKEUP, a.millisToNextAlarm(calendar, justFinished),
                    PendingIntent.getBroadcast(mAppContext, a.getID(), i, PendingIntent.FLAG_UPDATE_CURRENT));
        }

        a.set(true);

        showNotification(calendar);
    }

    private void disableAlarm(Alarm a, Calendar calendar) {
        Intent i = new Intent(INTENT_ACTION);
        mAlarmManager.cancel(PendingIntent.getBroadcast(mAppContext, a.getID(), i, PendingIntent.FLAG_UPDATE_CURRENT));

        a.set(false);

        if (alarmIsSet())
            showNotification(calendar);
        else
            cancelNotification();
    }

    private int generateAlarmID() {
        for (int id = 0; id < MAX_ALARMS; id++) {
            int i;
            for (i = 0; i < mAlarms.size(); i++) {
                if (mAlarms.get(i).getID() == id)
                    break;
            }
            if (i == mAlarms.size())
                return id;
        }
        return -1;
    }
}
