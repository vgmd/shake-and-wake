package com.matchamonkey.shakeandwake;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Formatter;

public class ShakeDurationPreference extends SeekBarPreference {

    public static final int SHAKE_DURATION_MIN = 1000;
    public static final int SHAKE_DURATION_MAX = 5000;

    private TextView mTextView;

    public ShakeDurationPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ShakeDurationPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ShakeDurationPreference(Context context) {
        super(context);
    }

    @Override
    protected View onCreateView(ViewGroup parent) {
        super.onCreateView(parent);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.preference_shake_duration, parent, false);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);

        mTextView = (TextView) view.findViewById(R.id.time);
        mTextView.setText(progressToTime());
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        if (restorePersistedValue) {
            mSeekBarProgress = getPersistedInt(DEFAULT_PROGRESS);
        } else {
            mSeekBarProgress = (Integer) defaultValue;
            persistInt((Integer) defaultValue);
        }
    }

    /**
     * Persists the current SeekBar progress
     * @param seekBar
     */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        persistInt(seekBar.getProgress());
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (mTextView != null)
            mTextView.setText(progressToTime());
    }

    /**
     * Converts SeekBar's progress to a String representing seconds.
     * One unit of progress is equal to 1/10th of a second.
     * @return A String in the form #s where # is the number of seconds to the first decimal.
     */
    private String progressToTime() {
        float ratio = ((float) mSeekBar.getProgress()) / 100.0f;
        float time = ((SHAKE_DURATION_MAX - SHAKE_DURATION_MIN) * ratio) + SHAKE_DURATION_MIN;
        time /= 1000.0f;

        StringBuilder sb = new StringBuilder();
        Formatter f = new Formatter(sb);
        f.format("%.1fs", time);
        return f.toString();
    }
}
