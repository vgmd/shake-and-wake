package com.matchamonkey.shakeandwake;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Alarm implements Comparable<Alarm> {

    private static final String JSON_ID = "id";
    private static final String JSON_DAYS = "days";
    private static final String JSON_HOUR = "hour";
    private static final String JSON_MIN = "min";
    private static final String JSON_SET = "set";

    public static final int SUNDAY    = 0x40;
    public static final int MONDAY    = 0x20;
    public static final int TUESDAY   = 0x10;
    public static final int WEDNESDAY = 0x08;
    public static final int THURSDAY  = 0x04;
    public static final int FRIDAY    = 0x02;
    public static final int SATURDAY  = 0x01;

    public static final int MS_PER_DAY = 86400000;
    public static final int MS_PER_HOUR = 3600000;
    public static final int MS_PER_MIN = 60000;
    public static final int MS_PER_SEC = 1000;

    private int mHour;
    private int mMin;
    private int mID;
    private boolean mIsSet;

    private int mDays;

    public Alarm(int hour, int min, int id, int days) {
        mHour = hour;
        mMin = min;
        mID = id;
        mDays = days;
        mIsSet = true;
    }

    public Alarm(JSONObject json) throws JSONException {
        mID = json.getInt(JSON_ID);
        mDays = json.getInt(JSON_DAYS);
        mHour = json.getInt(JSON_HOUR);
        mMin = json.getInt(JSON_MIN);
        mIsSet = json.getBoolean(JSON_SET);
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON_ID, mID);
        json.put(JSON_DAYS, mDays);
        json.put(JSON_HOUR, mHour);
        json.put(JSON_MIN, mMin);
        json.put(JSON_SET, mIsSet);
        return json;
    }

    public void set(boolean set) { mIsSet = set; }
    public void setHour(int hour) { mHour = hour; }
    public void setMin(int min) { mMin = min; }
    public void setDays(int days) { mDays = days; }

    public int getHour() { return mHour; }
    public int getMin() { return mMin; }
    public int getID() { return mID; }
    public int getDays() { return mDays; }
    public boolean isSet() { return mIsSet; }

    /**
     * @param cal an instance of java.util.Calendar with the current time according to device's
     *            time zone.
     * @param justFinished set true if the alarm just went off, false otherwise.
     * @return The number of milliseconds to the next alarm. 0 if no alarm is set.
     */
    public long millisToNextAlarm(Calendar cal, boolean justFinished) {
        long currentTimeInMillis = cal.getTimeInMillis();
        int currHour = cal.get(Calendar.HOUR_OF_DAY);
        int currMin = cal.get(Calendar.MINUTE);
        int currDay = cal.get(Calendar.DAY_OF_WEEK);
        int numDays = 0;

        if (mDays != 0) {
            numDays = -1;
            boolean days[] = new boolean[7];
            if ((mDays & Alarm.SUNDAY) > 0) days[0] = true;
            if ((mDays & Alarm.MONDAY) > 0) days[1] = true;
            if ((mDays & Alarm.TUESDAY) > 0) days[2] = true;
            if ((mDays & Alarm.WEDNESDAY) > 0) days[3] = true;
            if ((mDays & Alarm.THURSDAY) > 0) days[4] = true;
            if ((mDays & Alarm.FRIDAY) > 0) days[5] = true;
            if ((mDays & Alarm.SATURDAY) > 0) days[6] = true;

            // Calculate the number of days before the next alarm.
            // First, index into the days array using currDay and move forward.
            // Once the end of the array is reached AND numDays hasn't been set (i.e. < 0),
            // start from the beginning of the array until the current day.
            for (int i = currDay - 1; i < days.length; i++) {
                if (days[i]) {
                    // special case if alarm is set for the current day
                    if (i == currDay -1) {
                        if (currHour < mHour || (currHour == mHour && currMin <= mMin)) {
                            if (justFinished && currHour == mHour && currMin == mMin) {
                                numDays = 7;
                            } else {
                                numDays = 0;
                                break;
                            }
                        } else {
                            numDays = 7;
                        }
                    } else {
                        numDays = i - (currDay - 1);
                        break;
                    }
                }
            }
            if (numDays < 0 || numDays == 7) {
                for (int i = 0; i < currDay - 1; i++) {
                    if (days[i]) {
                        numDays = (7 - currDay) + (i + 1);
                        break;
                    }
                }
            }

            // numDays should be set at this point.
            if (numDays < 0) {
                throw new IllegalArgumentException("The number of days is an impossible value");
            }

            if (currHour > mHour || (currHour == mHour && currMin > mMin)) {
                numDays--;
                if (numDays < 0)
                    numDays = 6;
            }
        }

        int hours;
        int mins;
        if (mHour >= currHour)
            hours = mHour - currHour;
        else
            hours = 24 - currHour + mHour;

        if (mMin >= currMin)
            mins = mMin - currMin;
        else {
            mins = 60 - currMin + mMin;
            hours--;
            if (hours < 0)
                hours = 23;
        }

        int extraSeconds = cal.get(Calendar.SECOND);
        int extraMilliseconds = cal.get(Calendar.MILLISECOND);

        return currentTimeInMillis
                + numDays * MS_PER_DAY
                + hours * MS_PER_HOUR
                + mins * MS_PER_MIN
                - extraSeconds * MS_PER_SEC
                - extraMilliseconds;
    }

    public String toString(Calendar cal) {
        Date date = new Date(millisToNextAlarm(cal, false));
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, h:mm a", Locale.US);

        return sdf.format(date);
    }

    @Override
    public int compareTo(Alarm other) {
        if (this.mHour < other.mHour) {
            return -1;
        } else if (this.mHour > other.mHour) {
            return 1;
        } else if (this.mMin < other.mMin) {
            return -1;
        } else if (this.mMin > other.mMin) {
            return 1;
        } else {
            return 0;
        }
    }
}
