package com.matchamonkey.shakeandwake;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import java.util.Calendar;

public class PackageChangeService extends IntentService {

    public PackageChangeService() {
        super("PackageChangeService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent == null)
            return;

        String action = intent.getAction();
        if (!action.equals("android.intent.action.MY_PACKAGE_REPLACED"))
            return;

        Calendar cal = Calendar.getInstance();
        AlarmController.get(this, cal).loadAlarms(cal);

        PackageChangeReceiver.completeWakefulIntent(intent);
    }
}
